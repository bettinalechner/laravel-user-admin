<?php

Route::group(['middleware' => 'guest'], function() {
    Route::get('admin', 'Auth\AuthController@getLogin');
});

Route::group(['middleware' => 'auth'], function() {
    Route::resource('users', 'UserController', ['except' => ['show']]);
});

// Allow Self-Registration when environment variable is set to true
if (env('APP_ALLOW_REG', false) == true) {
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');
}

// Authentication
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::controllers([
    'password' => 'Auth\PasswordController'
]);
