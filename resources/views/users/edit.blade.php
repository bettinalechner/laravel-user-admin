@extends('app')

@section('content')

<div class="container">
    <div class="span4 shift4">
        <h1>Edit a User</h1>

        {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update', $user->id]]) !!}
            @include('_errors')

            <p>
                {!! Form::label('name') !!}
                {!! Form::text('name') !!}
            </p>

            <p>
                {!! Form::label('email') !!}
                {!! Form::input('email', 'email') !!}
            </p>

            <p><i>Users have to change their own passwords by using the "Reset Password" link on the log in page.</i></p>

            <p>
                {!! Form::submit('Save') !!}
            </p>
        {!! Form::close() !!}
    </div>
</div>

@endsection