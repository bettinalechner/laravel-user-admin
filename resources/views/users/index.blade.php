@extends('app')

@section('content')

<h1>User Administration</h1>

<a href="{{ action('UserController@create') }}">Add New User</a>

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Type</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->type }}</td>
                <td>
                    {!! Form::model($user, [
                        'method' => 'DELETE',
                        'class' => 'delete-confirm',
                        'action' => [
                            'UserController@destroy',
                            $user->id
                        ]
                    ]) !!}
                        <a href="{{ action('UserController@edit', [$user->id]) }}">Edit</a>
                        <button type="submit">Delete</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection