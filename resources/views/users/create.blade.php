@extends('app')

@section('content')

<div class="container">
    <div class="span4 shift4">
        <h1>Create User Account</h1>

        @include('_errors')

        <fieldset>
            {!! Form::open(['action' => 'UserController@store']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <label for="name">Name</label>
                <input type="text" id="name" name="name" value="{{ old('name') }}">

                <label for="email">E-Mail Address</label>
                <input type="email" id="email" name="email" value="{{ old('email') }}">

                <label for="password">Password</label>
                <input type="password" id="password" name="password">

                <label for="password_confirmation">Confirm Password</label>
                <input type="password" id="password_confirmation" name="password_confirmation">

                <label for="type">Type</label>
                <select name="type" id="type">
                    <option value="1">Admin</option>
                    <option value="2">Regular User</option>
                </select>

                <button type="submit">Register</button>
            {!! Form::close() !!}
        </fieldset>
    </div>
</div>

@endsection
